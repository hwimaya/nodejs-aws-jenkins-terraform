variable "RDS_PASSWORD" {
  default = "somepassword"
}

variable "API_PORT" {
  default = "5432"
}

variable "WEB_PORT" {
  default = "8080"
}

variable "AWS_REGION" {
  default = "ap-northeast-2"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "~/.ssh/id_rsa"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "~/.ssh/id_rsa.pub"
}

variable "AMIS" {
  type = map(string)
  default = {
    us-east-1      = "ami-0f9cf087c1f27d9b1"
    us-west-2      = "ami-0653e888ec96eab9b"
    ap-northeast-2 = "ami-0eee4dcc71fced4cf"
  }
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}

