terraform {
  backend "s3" {
    bucket = "build-landing-aws-jenkins-terraform-hwimaya"
    key    = "node-aws-jenkins-terraform.tfstat"
    region = "ap-northeast-2"
  }
}

