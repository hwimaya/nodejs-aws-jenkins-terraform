resource "aws_key_pair" "hwimaya-mykeypair" {
  key_name   = "hwimaya-mykeypair"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
  lifecycle {
    ignore_changes = [public_key]
  }
}

