provider "aws" {
  region                  = "ap-northeast-2"
  shared_credentials_file = "C:/Users/75005/.aws/credentials"
}

resource "aws_instance" "hwimaya-jenkins-instance" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = aws_subnet.hwimaya-main-public-1.id

  # the security group
  vpc_security_group_ids = [aws_security_group.hwimaya-jenkins-securitygroup.id]

  # the public SSH key
  key_name = aws_key_pair.hwimaya-mykeypair.key_name

  # user data
  user_data = data.template_cloudinit_config.cloudinit-jenkins.rendered

  iam_instance_profile = aws_iam_instance_profile.hwimaya-Jenkins-iam-role-instanceprofile.name
}

resource "aws_ebs_volume" "hwimaya-jenkins-data" {
  availability_zone = "ap-northeast-2a"
  size              = 20
  type              = "gp2"
  tags = {
    Name = "hwimaya-jenkins-data"
  }
}

resource "aws_volume_attachment" "hwimaya-jenkins-data-attachment" {
  device_name  = var.INSTANCE_DEVICE_NAME
  volume_id    = aws_ebs_volume.hwimaya-jenkins-data.id
  instance_id  = aws_instance.hwimaya-jenkins-instance.id
  skip_destroy = true
}

output "jenkins-ip" {
  value = [aws_instance.hwimaya-jenkins-instance.*.public_ip]
}

