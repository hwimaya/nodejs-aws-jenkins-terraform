# Internet VPC
resource "aws_vpc" "hwimaya-main" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "hwimaya-main"
  }
}

# Subnets
resource "aws_subnet" "hwimaya-main-public-1" {
  vpc_id                  = aws_vpc.hwimaya-main.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "ap-northeast-2a"

  tags = {
    Name = "hwimaya-main-public-1"
  }
}

# Internet GW
resource "aws_internet_gateway" "hwimaya-main-gw" {
  vpc_id = aws_vpc.hwimaya-main.id

  tags = {
    Name = "hwimaya-main"
  }
}

# route tables  
resource "aws_route_table" "hwimaya-main-public" {
  vpc_id = aws_vpc.hwimaya-main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.hwimaya-main-gw.id
  }

  tags = {
    Name = "hwimaya-main-public-1"
  }
}

# route associations public
resource "aws_route_table_association" "hwimaya-main-public-1-a" {
  subnet_id      = aws_subnet.hwimaya-main-public-1.id
  route_table_id = aws_route_table.hwimaya-main-public.id
}

